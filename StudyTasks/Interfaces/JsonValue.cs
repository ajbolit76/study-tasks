﻿namespace StudyTasks.Interfaces;

public interface IJsonValue
{
    string Encode();
}