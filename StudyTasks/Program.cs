﻿// See https://aka.ms/new-console-template for more information

using StudyTasks.Implementations;

var obj = new JsonObj(6);
obj.SetValue("key", new JsonStr("value"));
obj.SetValue("null", new JsonNull());
obj.SetValue("int", new JsonInt(5));
obj.SetValue("bool", new JsonBool(false));

var arr = new JsonArr(3);
arr.SetValue(0, new JsonInt(5));
arr.SetValue(1, new JsonNull());
arr.SetValue(2, new JsonInt(6));

obj.SetValue("array", arr);

var obj2 = new JsonObj(2);
obj2.SetValue("key1",new JsonStr("val"));
obj2.SetValue("key2",new JsonBool(false));

obj.SetValue("object", obj2);

Console.WriteLine(obj.Encode());