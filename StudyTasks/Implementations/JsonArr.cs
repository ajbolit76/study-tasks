﻿using System.Text;
using StudyTasks.Interfaces;

namespace StudyTasks.Implementations;

public class JsonArr : IJsonValue
{
    public int Length { get; }
    private readonly IJsonValue[] _values;

    public JsonArr(int length)
    {
        Length = length;
        _values = new IJsonValue[length];
    }

    public void SetValue(int index, IJsonValue value)
    {
        if (index >= Length || index < 0) throw new ArgumentException("Индекс за пределами массива", nameof(index));
        _values[index] = value;
    }
    
    public IJsonValue GetValue(int index)
    {
        if (index >= Length || index < 0) throw new ArgumentException("Индекс за пределами массива", nameof(index));
        return _values[index];
    }

    public string Encode()
    {
        var sb = new StringBuilder();
        
        sb.Append('[');
        
        for (int i = 0; i < _values.Length; i++)
        {
            var item = _values[i];
            sb.Append(item.Encode());
            if (i != _values.Length - 1)
                sb.Append(", ");
        }
        
        sb.Append(']');
        
        return sb.ToString();
    }
}