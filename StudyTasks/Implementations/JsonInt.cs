﻿using StudyTasks.Interfaces;

namespace StudyTasks.Implementations;

public class JsonInt : IJsonValue
{
    public int Value { get; set; }

    public JsonInt(int value)
    {
        Value = value;
    }

    public string Encode()
    {
        return Value.ToString();
    }
}