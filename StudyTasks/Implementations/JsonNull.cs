﻿using StudyTasks.Interfaces;

namespace StudyTasks.Implementations;

public class JsonNull : IJsonValue
{
    public string Encode()
    {
        return "null";
    }
}