﻿using System.Text;
using StudyTasks.Interfaces;

namespace StudyTasks.Implementations;

public class JsonObj : IJsonValue
{
    private Dictionary<string, IJsonValue> _values;

    public JsonObj(int length)
    {
        _values = new Dictionary<string, IJsonValue>(length);
    }
    
    public void SetValue(string name, IJsonValue value)
    {
        _values[name] = value;
    }
    
    public IJsonValue? GetValue(string name)
    {
        return _values.TryGetValue(name, out var result) ? result : null;
    }
    
    public string Encode()
    {
        var sb = new StringBuilder();
        sb.Append("{\n")
            .Append(string.Join(",\n", _values.Select(x=>$"\"{x.Key}\": {x.Value.Encode()}")))
            .Append("\n}");
        return sb.ToString();
    }
}