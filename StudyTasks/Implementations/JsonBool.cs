﻿using StudyTasks.Interfaces;

namespace StudyTasks.Implementations;

public class JsonBool : IJsonValue
{
    public bool Value { get; set; }

    public JsonBool(bool value)
    {
        Value = value;
    }

    public string Encode()
    {
        return Value ? "true" : "false";
    }
}