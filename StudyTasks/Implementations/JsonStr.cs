﻿using StudyTasks.Interfaces;

namespace StudyTasks.Implementations;

public class JsonStr : IJsonValue
{
    public string Value { get; set; }

    public JsonStr(string value)
    {
        Value = value;
    }

    public string Encode()
    {
        return $"\"{Value.Replace("\"","\\\"")}\"";
    }
}